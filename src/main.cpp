/*=========================================================
                                            CHI_PI3 - Example Usage
This program demonstrates the basic usage of the CHI_PI3 API for the 
Raspberry Pi3. The program will blink an LED if connected appropriately or 
can even activate the LED upon a button push via GPIO27.
===========================================================*/
#include<iostream>
#include <unistd.h>

#include "CHI_PI3/chi_pi3.h"

int main()
{
	CHI_PI3 pie;
	pie.ExportPin(17);
	pie.ExportPin(27);

	pie.SetPinMode(17,PI3_WRITE);
	pie.SetPinMode(27,PI3_READ);

	pie.SetPinValue(17,PI3_LOW);
	bool low=true;
	int pin27=0;

	
	while (true)
	{
		usleep(100000);
		pin27 = pie.GetPinValue(27);
		printf("Pin 27: %d\n",pin27);

		if (low)
		{
			pie.SetPinValue(17,PI3_HIGH);
			low=false;
		}
		else
		{
			pie.SetPinValue(17,PI3_LOW);
			low=true;
		}
	}

	return 0;
}
