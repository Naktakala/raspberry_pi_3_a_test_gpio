#Chi-Tech's Raspberry pi 3b API #
![alt text](doc/images/GPIO2.png "GPIO Quick Reference")

## Step 1 - Unboxing and Prepping
If you received the Raspberry Pi (Pi - from now on) in a kit, unbox it away from sources
of liquid or away from metallic components.

![alt text](doc/images/20171119_171453.jpg "Typical Boxed kit")

## Step 2 - Locate the Pi board and SD Card ##
The SD card will be the hard drive for the Pi (yes its tiny) so take care not to bend it. The slot for the SD
card is beneath the board. Insert the card with its exposed metallic components facing the
PCB.

![alt text](doc/images/20171119_171636.jpg "SD card bag")
![alt text](doc/images/20171119_171854.jpg "Board taken out of its bag")

## Step 3 - Be careful of not electrocuting yourself!! ##
Joke. The Pi has at max 5V to offer and very little power. Even if you try the 5V is not
sufficient to overcome the di-electric properties of your skin. Just like how you can hold the
poles of a 12V car battery and not get shocked (I foresee more science experiments, make sure to
  drink beer or braveness).

## Step 4 - Hookup a USB keyboard, USB mouse and HDMI monitor to the Pie ##
If this does not make it power on, try connecting the power too (micro-USB). Initially the Pie
will show a multi-color square. Wait for the installation menu to appear. The OS pre-installed on
the SD card will appear in the list.

![alt text](doc/images/20171119_172116.jpg "Initial Setup")

## Step 5 - Connect to the Wi-Fi and install the latest recommended version from the internet ##
If you don't do this immediately you will have trouble with some Wi-Fi networks later.

![alt text](doc/images/20171119_172235.jpg "Recommended version")

The installation will take a while but unlike a Windows installation you don't have to be there to
babysit. Go enjoy your life for 15 min.

## Step 6 -  Marvel at the tinyness look alike of a REAL PC. ##
This Pi is small and very capable. You can watch youtube and do calculations. Click around a bit
to get used to the interface.

## Step 7 - Open the terminal and check if git is installed ##
Open the terminal by either clicking the icon on the taskbar or by pressing Ctl+Alt+T (like a hacker). Type:

```
git
```
Press enter. If the terminal goes about at a liberal diarrhea it means git is installed. If not it will show "Command not found" or something like that. Go google how to install git then.

## Step 8 - Make a workfolder on the desktop ##
Type the following commands:
```
cd Desktop/
```
Then make a directory of your choosing (i.e. "PiPrograms"):
```
mkdir PiPrograms
cd PiPrograms/
```


## Step 9 - Clone the API test program ##

```
git clone https://bitbucket.org/Naktakala/raspberry_pi_3_a_test_gpio
cd raspberry_pi_3_a_test_gpio
```

## Step 10 - Install cmake ##
```
sudo apt-get install cmake
```

## Step 11 - Go to build directory and run cmake ##
```
mkdir build
cd build/
cmake ../
```
## Step 12 - Go back to the previous directory and build the executable ##
```
cd ../
make
```
If the terminal displays errors, deal with it. If no errors there should be a message like "Built target TestGPIO".

## Step 13 - Open src/main.cpp and look at the source code ##
```
#include<iostream>
#include <unistd.h>

#include "CHI_PI3/chi_pi3.h"

int main()
{
	CHI_PI3 pie;
	pie.ExportPin(17);
	pie.ExportPin(27);

	pie.SetPinMode(17,PI3_WRITE);
	pie.SetPinMode(27,PI3_READ);

	pie.SetPinValue(17,PI3_LOW);
	bool low=true;
	int pin27=0;


	while (true)
	{
		usleep(100000);
		pin27 = pie.GetPinValue(27);
		printf("Pin 27: %d\n",pin27);

		if (low)
		{
			pie.SetPinValue(17,PI3_HIGH);
			low=false;
		}
		else
		{
			pie.SetPinValue(17,PI3_LOW);
			low=true;
		}
	}

	return 0;
}
```

## Step 14 - Change the pin numbers to what you want (requires remake) and run the executable ##
```
sudo build/TestGPIO
```
## Step 15 - Connect a resistor and LED for testing ##
Connect one girl-on-girl action (ok, ok, female-female jumper wire) from any GND pin to the short leg of a LED. Connect GPIO17 (or the pin you used for writing) to the resistor (which should be in series with the long leg of the LED), again using a female-female connector.

![alt text](doc/images/20171119_181451.jpg "Jumper wires and components")

## Step 16 - Run the program ##
Run the program as sudo with:

```
sudo build/TestGPIO
```
The program should have the following output:
```
Pin 27: 0
Pin 27: 0
Pin 27: 0
Pin 27: 0
Pin 27: 0
Pin 27: 0
Pin 27: 0
etc.
```
If you installed the LED and resistor correctly the LED should also be blinking.

![alt text](doc/images/20171119_181139.jpg "Blinking LED")

## Pulling ##
To pull the latest version use the following command:
```
git pull https://bitbucket.org/Naktakala/raspberry_pi_3_a_test_gpio master
```

## Enabling SPI ##
```
sudo raspi-config
```
Select "Interfacing Options".

Select "SPI" option.

Select "Yes" then "OK".

Select "Finish" (right arrow) and Reboot the Pi.
